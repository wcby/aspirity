<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticlesRequest;
use App\Http\Resources\ArticleResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CategoryWithResource;
use App\Models\Category;

class ApiCategoryController extends Controller
{
    public function all()
    {
        $categories = Category::get();
        return CategoryResource::collection($categories);
    }

    public function show($slug, $paginate = 10)
    {
        $categories = Category::where('slug', $slug)
            ->with('articles')
            ->paginate($paginate);
        return CategoryWithResource::collection($categories);
    }

    public function category($slug, ArticlesRequest $request)
    {
        $category = Category::where('slug', $slug)->first();
        $prePage = $request->input('perPage', 10);

        if ($category) {
            $articles = $category->articles()
                ->paginate($prePage);
            return response()->json([
                'articles' => ArticleResource::collection($articles),
                'total' => $articles->total(),
                'last_page' => $articles->lastPage(),
                'current_page' => $articles->currentPage(),
            ]);
        } else {
            return response()->json(['message' => 'Категория не найдена'], 404);
        }
    }
}