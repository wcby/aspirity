<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\TagResource;
use App\Models\Tag;

class ApiTagController extends Controller
{
    public function all()
    {
        $randomTags = Tag::whereHas('articles', function ($query) {
            $query->where('is_publish', true);
        })->inRandomOrder()->take(20)->get();
        return TagResource::collection($randomTags);
    }

    public function show($slug, $perPage = 10)
    {
        $tag = Tag::where('slug', $slug)
            ->whereHas('articles', function ($query) {
                $query->where('is_publish', true);
            })->firstOrFail();

        $articles = $tag->articles()->paginate($perPage);

        return [
            'tag' => $tag,
            'articles' => TagResource::collection($articles),
            'total' => $articles->total(),
        ];
    }
}