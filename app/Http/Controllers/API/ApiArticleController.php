<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticlesRequest;
use App\Http\Resources\ArticleCommentResource;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use App\Models\Category;


class ApiArticleController extends Controller
{
    public function all(ArticlesRequest $request)
    {
        $prePage = $request->input('perPage', 10);;
        $articles = Article::where('is_publish', true)->paginate($prePage);
        return response()->json([
            'articles' => ArticleResource::collection($articles),
            'total' => $articles->total(),
            'last_page' => $articles->lastPage(),
            'current_page' => $articles->currentPage(),
        ]);
    }

    public function show($slug, $slugSecond)
    {
        $category = Category::where('slug', $slug)->first();

        if ($category) {
            $articles = $category->articles()
                ->where('slug', $slugSecond)
                ->get();
            return ArticleResource::collection($articles);
        } else {
            return response()->json(['message' => 'Категория не найдена'], 404);
        }
    }

    public function comments($slug)
    {
        $article = Article::where('slug', $slug)->with('comments')->first();

        $comments = $article->comments;
        return ArticleCommentResource::collection($comments);
    }
}