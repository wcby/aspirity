<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticlesRequest;
use App\Http\Requests\CommentRequest;
use App\Http\Resources\ArticleResource;
use App\Http\Resources\CommentResource;
use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;


class ApiCommentController extends Controller
{
    public function save(CommentRequest $request)
    {
        $requestData = $request->all();
        $comment = Comment::create([
            'name' => strip_tags($requestData['name']),
            'comment' => strip_tags($requestData['comment']),
            'rang' => $requestData['rang'],
            'is_published' => true,
            'article_id' => $requestData['article_id'],
        ]);
        $comment->save();
        return response()->json($comment);
    }
}