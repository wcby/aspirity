<?php

namespace App\Http\Resources;

use App\Models\ArticleTag;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $tags = ArticleTag::where('article_id', $this->id)->get();
        $comments = Comment::where('article_id', $this->id)->get();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'icon' => $this->icon,
            'subtitle' => $this->subtitle,
            'description' => $this->description,
            'fullUrl' => url($this->fullUrl),
            'category' => new ArticleCategoryResource($this->category),
            'tags' => ArticleTagResource::collection($tags),
            'comments' => CommentResource::collection($comments),
        ];
    }
}
