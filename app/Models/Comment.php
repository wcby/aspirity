<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Comment extends Model
{
    use HasFactory;

    protected $table = 'comments';
    protected bool $guard = false;
    protected $fillable = [
        'name',
        'comment',
        'rang',
        'is_published',
        'article_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function article(): BelongsTo
    {
        return $this->belongsTo(Article::class);
    }
}
