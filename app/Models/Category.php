<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected bool $guard = false;
    protected $fillable = [
        'name',
        'slug',
        'description',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function getFullUrlAttribute(): string
    {
        return 'categories/'.$this->slug;
    }

    public function articles(): HasMany
    {
        return $this->hasMany(Article::class);
    }
}
