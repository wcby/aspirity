<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Image extends Model
{
    use HasFactory;

    protected $table = 'images';
    protected bool $guard = false;
    protected $fillable = [
        'url',
        'imageable_type',
        'imageable_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }

}
