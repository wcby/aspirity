<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ArticleTag extends Model
{
    use HasFactory;

    protected $table = 'article_tag';
    protected bool $guard = false;
    protected $fillable = [
        'article_id',
        'tag_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function article(): BelongsTo
    {
        return $this->belongsTo(Article::class);
    }

    public function tag(): BelongsTo
    {
        return $this->belongsTo(Tag::class);
    }
}
