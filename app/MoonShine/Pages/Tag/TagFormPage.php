<?php

declare(strict_types=1);

namespace App\MoonShine\Pages\Tag;

use MoonShine\Fields\Text;
use MoonShine\Pages\Crud\FormPage;

class TagFormPage extends FormPage
{
    public function fields(): array
    {
        return [
            Text::make('Название', 'name'),
        ];
    }

    protected function topLayer(): array
    {
        return [
            ...parent::topLayer()
        ];
    }

    protected function mainLayer(): array
    {
        return [
            ...parent::mainLayer()
        ];
    }

    protected function bottomLayer(): array
    {
        return [
            ...parent::bottomLayer()
        ];
    }
}
