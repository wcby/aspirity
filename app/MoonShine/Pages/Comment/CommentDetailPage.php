<?php

declare(strict_types=1);

namespace App\MoonShine\Pages\Comment;

use MoonShine\Fields\ID;
use MoonShine\Fields\Number;
use MoonShine\Fields\Relationships\BelongsTo;
use MoonShine\Fields\Switcher;
use MoonShine\Fields\Text;
use MoonShine\Fields\TinyMce;
use MoonShine\Pages\Crud\DetailPage;

class CommentDetailPage extends DetailPage
{
    public function fields(): array
    {
        return [
            ID::make()->sortable(),
            Text::make('Имя', 'name'),
            Number::make('Оценка', 'rang'),
            TinyMce::make('Комментарий', 'comment'),
            Switcher::make('Показывать', 'is_published'),
            BelongsTo::make('Статья', 'article'),
        ];
    }

    protected function topLayer(): array
    {
        return [
            ...parent::topLayer()
        ];
    }

    protected function mainLayer(): array
    {
        return [
            ...parent::mainLayer()
        ];
    }

    protected function bottomLayer(): array
    {
        return [
            ...parent::bottomLayer()
        ];
    }
}
