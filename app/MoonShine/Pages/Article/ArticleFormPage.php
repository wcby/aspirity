<?php

declare(strict_types=1);

namespace App\MoonShine\Pages\Article;

use App\MoonShine\Resources\ArticleTagResource;
use App\MoonShine\Resources\CategoryResource;
use App\MoonShine\Resources\TagResource;
use MoonShine\Fields\ID;
use MoonShine\Fields\Image;
use MoonShine\Fields\Relationships\BelongsTo;
use MoonShine\Fields\Relationships\BelongsToMany;
use MoonShine\Fields\Relationships\HasMany;
use MoonShine\Fields\Slug;
use MoonShine\Fields\Text;
use MoonShine\Fields\TinyMce;
use MoonShine\Pages\Crud\FormPage;

class ArticleFormPage extends FormPage
{
    public function fields(): array
    {
        return [
            Text::make( 'Название', 'name' ),
            Slug::make('slug')->from('name')->readonly()->unique(),
            BelongsTo::make('Категория', 'category')
                ->searchable()
                ->nullable()
                ->placeholder('Выберите категорию'),
            Image::make('Картинка', 'icon')
                ->allowedExtensions(['jpg', 'jpeg', 'png', 'svg'])
                ->removable(),
            TinyMce::make('Описание', 'description'),
            HasMany::make('Теги', 'tags', resource: new ArticleTagResource())
                ->fields([
                    BelongsTo::make('Тег', 'tag'),
                ])
                ->creatable(),
        ];
    }

    protected function topLayer(): array
    {
        return [
            ...parent::topLayer()
        ];
    }

    protected function mainLayer(): array
    {
        return [
            ...parent::mainLayer()
        ];
    }

    protected function bottomLayer(): array
    {
        return [
            ...parent::bottomLayer()
        ];
    }
}
