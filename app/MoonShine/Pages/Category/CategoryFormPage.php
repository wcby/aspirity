<?php

declare(strict_types=1);

namespace App\MoonShine\Pages\Category;

use MoonShine\Fields\Slug;
use MoonShine\Fields\Text;
use MoonShine\Fields\TinyMce;
use MoonShine\Pages\Crud\FormPage;

class CategoryFormPage extends FormPage
{
    public function fields(): array
    {
        return [
            Text::make('Название', 'name'),
            Slug::make('slug')->from('name')->readonly()->unique(),
            TinyMce::make('Описание', 'description'),
        ];
    }

    protected function topLayer(): array
    {
        return [
            ...parent::topLayer()
        ];
    }

    protected function mainLayer(): array
    {
        return [
            ...parent::mainLayer()
        ];
    }

    protected function bottomLayer(): array
    {
        return [
            ...parent::bottomLayer()
        ];
    }
}
