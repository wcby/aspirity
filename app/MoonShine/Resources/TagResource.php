<?php

declare(strict_types=1);

namespace App\MoonShine\Resources;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tag;
use App\MoonShine\Pages\Tag\TagIndexPage;
use App\MoonShine\Pages\Tag\TagFormPage;
use App\MoonShine\Pages\Tag\TagDetailPage;

use MoonShine\Resources\ModelResource;

class TagResource extends ModelResource
{
    protected string $model = Tag::class;
    protected string $column = 'name';
    protected string $title = 'Теги';

    public function pages(): array
    {
        return [
            TagIndexPage::make($this->title()),
            TagFormPage::make(
                $this->getItemID()
                    ? __('moonshine::ui.edit')
                    : __('moonshine::ui.add')
            ),
            TagDetailPage::make(__('moonshine::ui.show')),
        ];
    }

    public function rules(Model $item): array
    {
        return [];
    }
}
