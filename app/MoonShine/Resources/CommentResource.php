<?php

declare(strict_types=1);

namespace App\MoonShine\Resources;

use Illuminate\Database\Eloquent\Model;
use App\Models\Comment;
use App\MoonShine\Pages\Comment\CommentIndexPage;
use App\MoonShine\Pages\Comment\CommentFormPage;
use App\MoonShine\Pages\Comment\CommentDetailPage;

use MoonShine\Resources\ModelResource;

class CommentResource extends ModelResource
{
    protected string $model = Comment::class;
    protected string $column = 'name';
    protected string $title = 'Комментарии';

    public function pages(): array
    {
        return [
            CommentIndexPage::make($this->title()),
            CommentFormPage::make(
                $this->getItemID()
                    ? __('moonshine::ui.edit')
                    : __('moonshine::ui.add')
            ),
            CommentDetailPage::make(__('moonshine::ui.show')),
        ];
    }

    public function rules(Model $item): array
    {
        return [];
    }
}
