<?php

declare(strict_types=1);

namespace App\MoonShine\Resources;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\MoonShine\Pages\Category\CategoryIndexPage;
use App\MoonShine\Pages\Category\CategoryFormPage;
use App\MoonShine\Pages\Category\CategoryDetailPage;

use MoonShine\Resources\ModelResource;

class CategoryResource extends ModelResource
{
    protected string $model = Category::class;
    protected string $column = 'name';
    protected string $title = 'Категории';

    public function pages(): array
    {
        return [
            CategoryIndexPage::make($this->title()),
            CategoryFormPage::make(
                $this->getItemID()
                    ? __('moonshine::ui.edit')
                    : __('moonshine::ui.add')
            ),
            CategoryDetailPage::make(__('moonshine::ui.show')),
        ];
    }

    public function rules(Model $item): array
    {
        return [];
    }
}
