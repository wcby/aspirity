<?php

declare(strict_types=1);

namespace App\MoonShine\Resources;

use Illuminate\Database\Eloquent\Model;
use App\Models\Article;
use App\MoonShine\Pages\Article\ArticleIndexPage;
use App\MoonShine\Pages\Article\ArticleFormPage;
use App\MoonShine\Pages\Article\ArticleDetailPage;

use MoonShine\Resources\ModelResource;

class ArticleResource extends ModelResource
{
    protected string $model = Article::class;
    protected string $column = 'name';
    protected string $title = 'Статьи';

    public function pages(): array
    {
        return [
            ArticleIndexPage::make($this->title()),
            ArticleFormPage::make(
                $this->getItemID()
                    ? __('moonshine::ui.edit')
                    : __('moonshine::ui.add')
            ),
            ArticleDetailPage::make(__('moonshine::ui.show')),
        ];
    }

    public function rules(Model $item): array
    {
        return [];
    }
}
