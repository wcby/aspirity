<?php

declare(strict_types=1);

namespace App\MoonShine\Resources;

use Illuminate\Database\Eloquent\Model;
use App\Models\ArticleTag;

use MoonShine\Fields\Relationships\BelongsTo;
use MoonShine\Fields\Relationships\HasOne;
use MoonShine\Resources\ModelResource;
use MoonShine\Decorations\Block;
use MoonShine\Fields\ID;

class ArticleTagResource extends ModelResource
{
    protected string $model = ArticleTag::class;

    protected string $title = 'ArticleTags';

    public function fields(): array
    {
        return [
            Block::make([
                BelongsTo::make('Тег', 'tag', resource: new TagResource())
                    ->searchable()
                    ->nullable()
                    ->placeholder('Выберите Тег'),
                BelongsTo::make('Статья', 'article', resource: new ArticleResource())
                    ->searchable()
                    ->nullable()
                    ->placeholder('Выберите Статью'),
            ]),
        ];
    }

    public function rules(Model $item): array
    {
        return [];
    }
}
