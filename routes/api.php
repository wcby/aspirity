<?php

use App\Http\Controllers\API\ApiArticleController;
use App\Http\Controllers\API\ApiCategoryController;
use App\Http\Controllers\API\ApiCommentController;
use App\Http\Controllers\API\ApiTagController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/articles', [ApiArticleController::class, 'all']);
Route::get('/articles', [ApiArticleController::class, 'all']);
Route::get('/article/{slug}/comments', [ApiArticleController::class, 'comments']);
Route::get('/categories/{slug}/{slugSecond}', [ApiArticleController::class, 'show']);
Route::get('/categories', [ApiCategoryController::class, 'all']);
Route::get('/categories/{slug}', [ApiCategoryController::class, 'category']);
Route::get('/tags', [ApiTagController::class, 'all']);
Route::post('/comment/add', [ApiCommentController::class, 'save']);