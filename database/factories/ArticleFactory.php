<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ArticleFactory extends Factory
{
    protected $model = Article::class;

    public function definition()
    {
        $title = $this->faker->unique()->sentence(3);
        $slug = Str::slug($title);
        $description = $this->faker->paragraphs(2, true);
        $subtitle = $this->faker->paragraph(1); // Taking the first 100 characters of the description
        return [
            'name' => $title,
            'slug' => $slug,
            'description' => $description,
            'subtitle' => $subtitle,
            'is_publish' => $this->faker->boolean,
            'icon' => $this->faker->imageUrl(900, 300),
            'category_id' => function () {
                return Category::inRandomOrder()->first()->id;
            },
        ];
    }
}
