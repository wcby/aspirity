<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CategoryFactory extends Factory
{
    protected $model = Category::class;

    public function definition()
    {
        $title = $this->faker->unique()->word(1);
        $slug = Str::slug($title);
        return [
            'name' => $title,
            'slug' => $slug,
            'description' => $this->faker->paragraph(4),
        ];
    }
}
