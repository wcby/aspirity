<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\ArticleTag;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ArticleTag>
 */
class ArticleTagFactory extends Factory
{
    protected $model = ArticleTag::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $articleIds = Article::pluck('id')->toArray();
        $tagIds = Tag::pluck('id')->toArray();

        return [
            'article_id' => $this->faker->randomElement($articleIds),
            'tag_id' => $this->faker->randomElement($tagIds),
        ];
    }
}
