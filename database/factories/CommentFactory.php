<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CommentFactory extends Factory
{
    protected $model = Comment::class;

    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'comment' => $this->faker->paragraph(2, true),
            'is_published' => $this->faker->boolean,
            'rang' => $this->faker->numberBetween(0, 5),
            'article_id' => function () {
                return Article::inRandomOrder()->first()->id;
            },
        ];
    }
}
