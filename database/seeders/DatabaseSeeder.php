<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            TagSeeder::class,
            CategorySeeder::class,
            ArticleSeeder::class,
            ArticleTagSeeder::class,
            MoonshineUserSeeder::class,
            CommentSeeder::class,
        ]);
    }
}
