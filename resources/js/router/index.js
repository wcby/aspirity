import { createRouter, createWebHistory } from 'vue-router'

import HomeView from '@/views/HomeView.vue'
import CategoriesView from '@/views/CategoriesView.vue'
import CategoryView from '@/views/CategoryView.vue'
import ArticleView from '@/views/ArticleView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/categories',
    name: 'categories',
    component: CategoriesView
  },
  {
    path: '/categories/:slug',
    name: 'category.show',
    component: CategoryView
  },
  {
    path: '/categories/:slug/:slugSecond',
    name: 'article.show',
    component: ArticleView
  }
];

export default createRouter({
  history: createWebHistory(),
  routes
})